'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Dessert = function () {
    function Dessert(type, calories) {
        _classCallCheck(this, Dessert);

        this.type = type;
        this.calories = calories;
    }

    _createClass(Dessert, [{
        key: 'present',
        value: function present() {
            return ' This is an ' + this.type + ' it has this many cals ' + this.calories;
        }
    }]);

    return Dessert;
}();

var vanillaIceCream = function (_Dessert) {
    _inherits(vanillaIceCream, _Dessert);

    function vanillaIceCream(type, calories, scoops, flavor) {
        _classCallCheck(this, vanillaIceCream);

        var _this = _possibleConstructorReturn(this, (vanillaIceCream.__proto__ || Object.getPrototypeOf(vanillaIceCream)).call(this, type, calories));

        _this.flavor = flavor;
        _this.scoops = scoops;
        return _this;
    }

    _createClass(vanillaIceCream, [{
        key: 'show',
        value: function show() {
            return this.present() + ' it is ' + this.flavor + ' with this many scoops ' + this.scoops;
        }
    }, {
        key: 'includeSpoon',
        value: function includeSpoon() {
            console.log('here is your spoon!');
        }
    }]);

    return vanillaIceCream;
}(Dessert);

var myDessert = new vanillaIceCream(" Ice Cream ", 340, 3, " vanilla ");
document.getElementById("treat").innerHTML = myDessert.show();

myDessert.includeSpoon();

console.log(myDessert.show());