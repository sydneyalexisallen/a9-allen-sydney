class Dessert{
    constructor(type, calories){
        this.type = type;
        this.calories = calories;
    }

    present(){
        return ' This is an ' + this.type + ' it has this many cals ' + this.calories;
    }
}

class vanillaIceCream extends Dessert {
    constructor (type, calories, scoops, flavor){
        super(type, calories);
         this.flavor = flavor;
         this.scoops = scoops;
    }

    show(){
        return this.present() + ' it is ' + this.flavor + ' with this many scoops ' + this.scoops;
    }
   
    includeSpoon(){
        console.log ('here is your spoon!');
 
    }


}
    
   




let myDessert = new vanillaIceCream(" Ice Cream ", 340, 3, " vanilla ");
document.getElementById("treat").innerHTML = myDessert.show();

myDessert.includeSpoon();





console.log(myDessert.show());



